**Enlaces de refuerzo – ayuda**

**Foro del curso**
- Os he dejado un ejemplo de como se envian los mensajes en él [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/issues)


---
1. **LibreOffice**

- Instalador LibreOffice [LINK](https://es.libreoffice.org/descarga/libreoffice/?type=win-x86&version=7.1.3&lang=es)

- Manuales de LibreOffice [LINK](https://documentation.libreoffice.org/es/documentacion-en-espanol/)

-  Curso de LibreOffice Calc (repaso) [LINK](https://www.youtube.com/playlist?list=PLLLaU95AMQPrMifyMRgiwhqKA64g7Kiea)


---
2. **Microsoft Excel (365)**

-  Curso básico de Microsoft Excel [LINK](https://support.microsoft.com/es-es/office/aprendizajes-en-vídeo-de-excel-9bc05390-e94c-46af-a5b3-d7c22f6990bb?ui=es-es&rs=es-es&ad=es)

- Ayuda general de Microsoft Excel [LINK](https://support.microsoft.com/es-es/excel)


---
3. **Lenguaje de programación R**

-  **Instalador de lenguaje R** [LINK](https://cran.r-project.org/)
    
- [1] Windows [LINK](https://cran.r-project.org/bin/windows/base/)

- [2]  Mac [LINK](https://cran.r-project.org/bin/macosx/)

- [3]  Linux [LINK](https://cran.r-project.org/bin/linux/)

- **Instalador de Rstudio** [LINK](https://www.rstudio.com/products/rstudio/download/#download)


---
4. **Pagina de descarga de ejercicios**

- Kaggle [LINK](https://www.kaggle.com/)

- Data.world [LINK](https://data.world/datasets/open-data)

- Open data Euskadi [LINK](https://opendata.euskadi.eus/catalogo-datos/)

- Open data Bilbao [LINK](https://www.bilbao.eus/opendata/es/inicio)

- Data Planet [LINK](https://dataplanet.sagepub.com/)

- Data First [LINK](https://www.datafirst.uct.ac.za/dataportal/index.php/catalog/central)


---
5. **Manuales de programación y estadística**

- *Libros para Data Science*  [LINK](https://t.co/aipnnMNIKB)

- *Manuales libres (de todo tipo y niveles)* [LINK](https://bookdown.org/)

- Manuales libres de R [LINK](https://bookdown.org/home/tags/r-programming/)

- Manuales libres de R (avanzado) [LINK](https://bookdown.org/home/tags/advanced-r/)

- Manuales libres de Estadística [LINK](https://bookdown.org/home/tags/statistics/)

- Manuales libres de Data analysis [LINK](https://bookdown.org/home/tags/data-analysis/)

- Manuales libres de Analisis medicina [LINK](https://bookdown.org/home/tags/medicine/)

- Manuales libres de data science [LINK](https://bookdown.org/home/tags/data-science/)

- Computer Science courses  [LINK](https://github.com/Developer-Y/cs-video-courses)

- Coding for data  [LINK](https://matthew-brett.github.io/cfd2019/)


---
6. **Manuales de R (Selección)**
---
    A) Manuales de programación R:

- Manual de R (amigable 1) [LINK](https://fhernanb.github.io/Manual-de-R/)

- Manual de R (amigable 2) [LINK](https://bookdown.org/matiasandina/R-intro/)

- R para principiantes [LINK](https://bookdown.org/jboscomendoza/r-principiantes4/)

- Manual de Rstudio [LINK](https://bookdown.org/gboccardo/manual-ED-UCH/)


---
    B) Manuales de estadística

- Estadística básica [LINK](https://bookdown.org/aquintela/EBE/)

- Estadística Aplicada con R [LINK](https://bookdown.org/oscar_teach/estadistica_aplicada_con_r/)

- Estadística multivariada [LINK](https://est-mult.netlify.app/)

- R exploratory data analysis [LINK](https://bookdown.org/rdpeng/exdata/)

- Estadística y Machine Learning con R [LINK](https://github.com/JoaquinAmatRodrigo/Estadistica-con-R)

- Machine Learning for beginners [LINK](https://github.com/microsoft/ML-For-Beginners)

- Introducción al Machine Learning [LINK](https://code.datasciencedojo.com/Danielhuang69/tutorials/tree/master/Introduction%20to%20Machine%20Learning%20with%20R%20and%20Caret)

- Ciencia de datos [LINK](https://github.com/CristinaGil/Ciencia-de-Datos-R)

- Easystats [LINK](https://easystats.github.io/easystats/)

- Data Science Cheatsheet [LINK](https://github.com/ml874/Data-Science-Cheatsheet)

- R for Non-programmers [LINK](https://bookdown.org/daniel_dauber_io/r4np_book/)

- Intro to Probability for Data Science [LINK](https://probability4datascience.com/index.html)

- Introduction to Datascience: Learn Julia Programming, Math & Datascience from Scratch [LINK](https://datascience-book.gitlab.io/book.html)


---
    C) (Twitter)

- R Function a day [LINK](https://twitter.com/rfunctionaday)

- Data Science Dojo [LINK](https://twitter.com/DataScienceDojo)

- Ecology in R  [LINK](https://twitter.com/EcologyinR)

- Comunidad R Hispano [LINK](https://twitter.com/R_Hisp)

- Rstudio [LINK](https://twitter.com/rstudio)


---
7. **Graficos en R**

- R graph gallery [LINK](https://www.r-graph-gallery.com/index.html)

- Plotly en R [LINK](https://plotly.com/r/)

- Graficos con ggplot2 [LINK](https://bookdown.org/home/tags/ggplot2/)

- Hacer applicaciones interactuables con shiny (con botones y esas cositas) [LINK](https://deanattali.com/blog/building-shiny-apps-tutorial/)


---
8. **Paginas de bioinformatica**

- stackoverflow (foro de informatica para problemas/soluciones) [LINK](https://stackoverflow.com/)

- Bioinformatics [LINK](https://www.bioinformatics.org/)

- PSPP (similar al SPSS) [LINK](https://www.gnu.org/software/pspp/)

- Modeller [LINK](https://salilab.org/modeller/)

- Jalview [LINK](https://www.jalview.org/)

- Lista de software (Wiki) [LINK](https://en.wikipedia.org/wiki/List_of_open-source_bioinformatics_software)

- Otra lista [LINK](https://www.softwareradius.com/best-bioinformatics-software-and-tools/)

- Un poco de ayuda con todo [LINK](https://overapi.com/)




Correo para dudas/  preguntas / etc:
Mikel Aguirre Rodrigo:  mikel.aguirre@proton.me
